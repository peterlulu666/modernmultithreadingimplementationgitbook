<html>
<head><meta http-equiv=Content-Type content="text/html; charset=UTF-8">
<style type="text/css">
<!--
span.cls_016{font-family:"Comic Sans MS",serif;font-size:28.1px;color:rgb(51,51,204);font-weight:normal;font-style:normal;text-decoration: underline}
div.cls_016{font-family:"Comic Sans MS",serif;font-size:28.1px;color:rgb(51,51,204);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_004{font-family:Arial,serif;font-size:20.4px;color:rgb(51,51,204);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_004{font-family:Arial,serif;font-size:20.4px;color:rgb(51,51,204);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_005{font-family:Arial,serif;font-size:20.4px;color:rgb(66,77,213);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_005{font-family:Arial,serif;font-size:20.4px;color:rgb(66,77,213);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_007{font-family:"Comic Sans MS",serif;font-size:24.1px;color:rgb(0,127,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_007{font-family:"Comic Sans MS",serif;font-size:24.1px;color:rgb(0,127,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_006{font-family:"Comic Sans MS",serif;font-size:24.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_006{font-family:"Comic Sans MS",serif;font-size:24.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_002{font-family:Times,serif;font-size:14.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_002{font-family:Times,serif;font-size:14.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_017{font-family:Arial,serif;font-size:28.1px;color:rgb(66,77,213);font-weight:normal;font-style:normal;text-decoration: underline}
div.cls_017{font-family:Arial,serif;font-size:28.1px;color:rgb(66,77,213);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_018{font-family:"Comic Sans MS",serif;font-size:24.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: underline}
div.cls_018{font-family:"Comic Sans MS",serif;font-size:24.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_009{font-family:"Comic Sans MS",serif;font-size:20.1px;color:rgb(204,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_009{font-family:"Comic Sans MS",serif;font-size:20.1px;color:rgb(204,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_010{font-family:"Comic Sans MS",serif;font-size:20.1px;color:rgb(51,51,204);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_010{font-family:"Comic Sans MS",serif;font-size:20.1px;color:rgb(51,51,204);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_012{font-family:"Comic Sans MS",serif;font-size:16.1px;color:rgb(204,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_012{font-family:"Comic Sans MS",serif;font-size:16.1px;color:rgb(204,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_011{font-family:"Comic Sans MS",serif;font-size:20.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_011{font-family:"Comic Sans MS",serif;font-size:20.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_019{font-family:"Comic Sans MS",serif;font-size:18.1px;color:rgb(0,127,0);font-weight:normal;font-style:normal;text-decoration: underline}
div.cls_019{font-family:"Comic Sans MS",serif;font-size:18.1px;color:rgb(0,127,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_013{font-family:"Comic Sans MS",serif;font-size:18.1px;color:rgb(0,127,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_013{font-family:"Comic Sans MS",serif;font-size:18.1px;color:rgb(0,127,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_014{font-family:"Comic Sans MS",serif;font-size:18.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_014{font-family:"Comic Sans MS",serif;font-size:18.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_015{font-family:"Comic Sans MS",serif;font-size:18.1px;color:rgb(51,51,204);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_015{font-family:"Comic Sans MS",serif;font-size:18.1px;color:rgb(51,51,204);font-weight:normal;font-style:normal;text-decoration: none}
-->
</style>
<script type="text/javascript" src="TracingReplayforSharedVariables/wz_jsgraphics.js"></script>
</head>
<body>
<div style="position:absolute;left:50%;margin-left:-396px;top:0px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="TracingReplayforSharedVariables/background01.jpg" width=792 height=612></div>
<div style="position:absolute;left:85.27px;top:60.08px" class="cls_016"><span class="cls_016">Tracing/Replay for Shared Variables</span></div>
<div style="position:absolute;left:85.27px;top:121.23px" class="cls_004"><span class="cls_004">q</span><span class="cls_005"> </span><span class="cls_007">Introduction</span></div>
<div style="position:absolute;left:85.27px;top:163.63px" class="cls_004"><span class="cls_004">q</span><span class="cls_005"> </span><span class="cls_006">RW-Sequence</span></div>
<div style="position:absolute;left:85.27px;top:207.63px" class="cls_004"><span class="cls_004">q</span><span class="cls_005"> </span><span class="cls_006">Tracing and Replay</span></div>
<div style="position:absolute;left:85.27px;top:250.63px" class="cls_004"><span class="cls_004">q</span><span class="cls_005"> </span><span class="cls_006">Implementation Issues</span></div>
<div style="position:absolute;left:86.77px;top:543.60px" class="cls_002"><span class="cls_002">Advanced Topics in Software Engineering</span></div>
<div style="position:absolute;left:662.77px;top:543.60px" class="cls_002"><span class="cls_002">1</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:622px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="TracingReplayforSharedVariables/background02.jpg" width=792 height=612></div>
<div style="position:absolute;left:85.27px;top:60.08px" class="cls_016"><span class="cls_016">Tracing and Replay</span></div>
<div style="position:absolute;left:85.27px;top:121.23px" class="cls_007"><span class="cls_007">Replay</span><span class="cls_006"> is to repeat the behavior of a previous</span></div>
<div style="position:absolute;left:85.27px;top:149.23px" class="cls_006"><span class="cls_006">execution of a concurrent program.</span></div>
<div style="position:absolute;left:85.27px;top:192.63px" class="cls_007"><span class="cls_007">Tracing</span><span class="cls_006"> is to capture necessary information during a</span></div>
<div style="position:absolute;left:85.27px;top:221.23px" class="cls_006"><span class="cls_006">concurrent execution such that it can be repeated</span></div>
<div style="position:absolute;left:85.27px;top:250.23px" class="cls_006"><span class="cls_006">later in a controlled fashion.</span></div>
<div style="position:absolute;left:86.77px;top:543.60px" class="cls_002"><span class="cls_002">Advanced Topics in Software Engineering</span></div>
<div style="position:absolute;left:662.77px;top:543.60px" class="cls_002"><span class="cls_002">2</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:1244px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="TracingReplayforSharedVariables/background03.jpg" width=792 height=612></div>
<div style="position:absolute;left:85.27px;top:60.08px" class="cls_016"><span class="cls_016">Challenges</span></div>
<div style="position:absolute;left:85.27px;top:121.23px" class="cls_006"><span class="cls_006">There are a number of challenges to address for</span></div>
<div style="position:absolute;left:85.27px;top:149.23px" class="cls_006"><span class="cls_006">tracing and replay:</span></div>
<div style="position:absolute;left:85.27px;top:192.63px" class="cls_004"><span class="cls_004">q</span><span class="cls_005"> </span><span class="cls_006">What exactly should be replayed?</span></div>
<div style="position:absolute;left:85.27px;top:235.63px" class="cls_004"><span class="cls_004">q</span><span class="cls_005"> </span><span class="cls_006">What information should be captured?</span></div>
<div style="position:absolute;left:85.27px;top:279.63px" class="cls_004"><span class="cls_004">q</span><span class="cls_005"> </span><span class="cls_006">How should such information be encoded?</span></div>
<div style="position:absolute;left:86.77px;top:543.60px" class="cls_002"><span class="cls_002">Advanced Topics in Software Engineering</span></div>
<div style="position:absolute;left:662.77px;top:543.60px" class="cls_002"><span class="cls_002">3</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:1866px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="TracingReplayforSharedVariables/background04.jpg" width=792 height=612></div>
<div style="position:absolute;left:85.27px;top:60.08px" class="cls_016"><span class="cls_016">SYN-sequence</span></div>
<div style="position:absolute;left:85.27px;top:121.23px" class="cls_006"><span class="cls_006">A </span><span class="cls_007">SYN-sequence</span><span class="cls_006"> is a sequence of synchronization</span></div>
<div style="position:absolute;left:85.27px;top:149.23px" class="cls_006"><span class="cls_006">operations, i.e. operations that are performed on</span></div>
<div style="position:absolute;left:85.27px;top:178.23px" class="cls_006"><span class="cls_006">shared variables or synchronization constructs.</span></div>
<div style="position:absolute;left:85.27px;top:221.63px" class="cls_006"><span class="cls_006">A concurrent execution can be repeated, in terms of</span></div>
<div style="position:absolute;left:85.27px;top:250.23px" class="cls_006"><span class="cls_006">producing the same results, if the same </span><span class="cls_007">SYN-</span></div>
<div style="position:absolute;left:85.27px;top:279.23px" class="cls_007"><span class="cls_007">sequence</span><span class="cls_006"> is replayed.</span></div>
<div style="position:absolute;left:86.77px;top:543.60px" class="cls_002"><span class="cls_002">Advanced Topics in Software Engineering</span></div>
<div style="position:absolute;left:662.77px;top:543.60px" class="cls_002"><span class="cls_002">4</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:2488px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="TracingReplayforSharedVariables/background05.jpg" width=792 height=612></div>
<div style="position:absolute;left:85.27px;top:60.08px" class="cls_016"><span class="cls_016">SYN-sequence (cont</span><span class="cls_017">’</span><span class="cls_016">d)</span></div>
<div style="position:absolute;left:85.27px;top:121.23px" class="cls_006"><span class="cls_006">A SYN-sequence can be </span><span class="cls_007">object-based </span><span class="cls_006">or </span><span class="cls_007">thread-</span></div>
<div style="position:absolute;left:85.27px;top:149.23px" class="cls_007"><span class="cls_007">based</span><span class="cls_006">.</span></div>
<div style="position:absolute;left:85.27px;top:192.63px" class="cls_006"><span class="cls_006">An object-based SYN-sequence is associated with a</span></div>
<div style="position:absolute;left:85.27px;top:221.23px" class="cls_006"><span class="cls_006">shared </span><span class="cls_007">object</span><span class="cls_006">, consisting of all synchronization</span></div>
<div style="position:absolute;left:85.27px;top:250.23px" class="cls_006"><span class="cls_006">operations that are executed on that object.</span></div>
<div style="position:absolute;left:85.27px;top:293.63px" class="cls_006"><span class="cls_006">A thread-based SYN-sequence is associated with a</span></div>
<div style="position:absolute;left:85.27px;top:322.23px" class="cls_007"><span class="cls_007">thread</span><span class="cls_006">, consisting of all synchronization operations</span></div>
<div style="position:absolute;left:85.27px;top:351.23px" class="cls_006"><span class="cls_006">that are executed by that thread.</span></div>
<div style="position:absolute;left:86.77px;top:543.60px" class="cls_002"><span class="cls_002">Advanced Topics in Software Engineering</span></div>
<div style="position:absolute;left:662.77px;top:543.60px" class="cls_002"><span class="cls_002">5</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:3110px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="TracingReplayforSharedVariables/background06.jpg" width=792 height=612></div>
<div style="position:absolute;left:85.27px;top:60.08px" class="cls_016"><span class="cls_016">SYN-sequence (cont</span><span class="cls_017">’</span><span class="cls_016">d)</span></div>
<div style="position:absolute;left:85.27px;top:121.23px" class="cls_006"><span class="cls_006">Replay based on SYN-sequences can be accomplished</span></div>
<div style="position:absolute;left:85.27px;top:149.23px" class="cls_006"><span class="cls_006">by inserting additional control into programming</span></div>
<div style="position:absolute;left:85.27px;top:178.23px" class="cls_006"><span class="cls_006">objects.</span></div>
<div style="position:absolute;left:85.27px;top:221.63px" class="cls_006"><span class="cls_006">The alternative is to record thread schedules, i.e.,</span></div>
<div style="position:absolute;left:85.27px;top:250.23px" class="cls_006"><span class="cls_006">we record information about how context switches</span></div>
<div style="position:absolute;left:85.27px;top:279.23px" class="cls_006"><span class="cls_006">are performed.</span></div>
<div style="position:absolute;left:85.27px;top:322.63px" class="cls_006"><span class="cls_006">Replay based on </span><span class="cls_007">thread schedules</span><span class="cls_006"> often involve</span></div>
<div style="position:absolute;left:85.27px;top:351.23px" class="cls_006"><span class="cls_006">modifications to the thread scheduler, which is</span></div>
<div style="position:absolute;left:85.27px;top:380.23px" class="cls_006"><span class="cls_006">usually more difficult to implement, but can be more</span></div>
<div style="position:absolute;left:85.27px;top:409.23px" class="cls_006"><span class="cls_006">efficient.</span></div>
<div style="position:absolute;left:86.77px;top:543.60px" class="cls_002"><span class="cls_002">Advanced Topics in Software Engineering</span></div>
<div style="position:absolute;left:662.77px;top:543.60px" class="cls_002"><span class="cls_002">6</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:3732px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="TracingReplayforSharedVariables/background07.jpg" width=792 height=612></div>
<div style="position:absolute;left:85.27px;top:60.08px" class="cls_016"><span class="cls_016">Tracing/Replay for Shared Variables</span></div>
<div style="position:absolute;left:85.27px;top:121.23px" class="cls_004"><span class="cls_004">q</span><span class="cls_005"> </span><span class="cls_006">Introduction</span></div>
<div style="position:absolute;left:85.27px;top:163.63px" class="cls_004"><span class="cls_004">q</span><span class="cls_005"> </span><span class="cls_007">RW-sequence</span></div>
<div style="position:absolute;left:85.27px;top:207.63px" class="cls_004"><span class="cls_004">q</span><span class="cls_005"> </span><span class="cls_006">Tracing and Replay</span></div>
<div style="position:absolute;left:85.27px;top:250.63px" class="cls_004"><span class="cls_004">q</span><span class="cls_005"> </span><span class="cls_006">Implementation Issues</span></div>
<div style="position:absolute;left:86.77px;top:543.60px" class="cls_002"><span class="cls_002">Advanced Topics in Software Engineering</span></div>
<div style="position:absolute;left:662.77px;top:543.60px" class="cls_002"><span class="cls_002">7</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:4354px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="TracingReplayforSharedVariables/background08.jpg" width=792 height=612></div>
<div style="position:absolute;left:85.27px;top:60.08px" class="cls_016"><span class="cls_016">Definition</span></div>
<div style="position:absolute;left:85.27px;top:121.23px" class="cls_006"><span class="cls_006">A SYN-sequence for a shared variable is a sequence</span></div>
<div style="position:absolute;left:85.27px;top:149.23px" class="cls_006"><span class="cls_006">of read and write operations, called a </span><span class="cls_007">RW-sequence</span><span class="cls_006">.</span></div>
<div style="position:absolute;left:85.27px;top:192.63px" class="cls_006"><span class="cls_006">Each </span><span class="cls_007">write</span><span class="cls_006"> operation on a shared variable creates a</span></div>
<div style="position:absolute;left:85.27px;top:221.23px" class="cls_006"><span class="cls_006">new version of the variable.</span></div>
<div style="position:absolute;left:85.27px;top:264.63px" class="cls_006"><span class="cls_006">An execution can be replayed by ensuring each</span></div>
<div style="position:absolute;left:85.27px;top:293.23px" class="cls_006"><span class="cls_006">thread </span><span class="cls_007">reads</span><span class="cls_006"> and </span><span class="cls_007">writes</span><span class="cls_006"> the same variable versions</span></div>
<div style="position:absolute;left:85.27px;top:322.23px" class="cls_006"><span class="cls_006">that were recorded in the execution trace.</span></div>
<div style="position:absolute;left:86.77px;top:543.60px" class="cls_002"><span class="cls_002">Advanced Topics in Software Engineering</span></div>
<div style="position:absolute;left:662.77px;top:543.60px" class="cls_002"><span class="cls_002">8</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:4976px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="TracingReplayforSharedVariables/background09.jpg" width=792 height=612></div>
<div style="position:absolute;left:85.27px;top:60.08px" class="cls_016"><span class="cls_016">Encoding</span></div>
<div style="position:absolute;left:85.27px;top:121.23px" class="cls_006"><span class="cls_006">For each variable, we keep track of its versions: a</span></div>
<div style="position:absolute;left:85.27px;top:149.23px" class="cls_007"><span class="cls_007">write</span><span class="cls_006"> operation increases the version number; a </span><span class="cls_007">read</span></div>
<div style="position:absolute;left:85.27px;top:178.23px" class="cls_006"><span class="cls_006">operation keeps the version number unchanged.</span></div>
<div style="position:absolute;left:85.27px;top:221.63px" class="cls_006"><span class="cls_006">In a RW-sequence, a read event is encoded as </span><span class="cls_007">(ID,</span></div>
<div style="position:absolute;left:85.27px;top:250.23px" class="cls_007"><span class="cls_007">version),</span><span class="cls_006"> where </span><span class="cls_007">ID</span><span class="cls_006"> is the </span><span class="cls_007">ID </span><span class="cls_006">of the thread that</span></div>
<div style="position:absolute;left:85.27px;top:279.23px" class="cls_006"><span class="cls_006">executes the read event, and </span><span class="cls_007">version</span><span class="cls_006"> is the current</span></div>
<div style="position:absolute;left:85.27px;top:308.23px" class="cls_006"><span class="cls_006">version number.</span></div>
<div style="position:absolute;left:86.77px;top:543.60px" class="cls_002"><span class="cls_002">Advanced Topics in Software Engineering</span></div>
<div style="position:absolute;left:662.77px;top:543.60px" class="cls_002"><span class="cls_002">9</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:5598px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="TracingReplayforSharedVariables/background10.jpg" width=792 height=612></div>
<div style="position:absolute;left:85.27px;top:60.08px" class="cls_016"><span class="cls_016">Encoding (cont</span><span class="cls_017">’</span><span class="cls_016">d)</span></div>
<div style="position:absolute;left:85.27px;top:121.23px" class="cls_006"><span class="cls_006">A write event is encoded as </span><span class="cls_007">(ID, version, total</span></div>
<div style="position:absolute;left:85.27px;top:149.23px" class="cls_007"><span class="cls_007">reads)</span><span class="cls_006">, where </span><span class="cls_007">ID</span><span class="cls_006"> is the ID of the thread that</span></div>
<div style="position:absolute;left:85.27px;top:178.23px" class="cls_006"><span class="cls_006">executes the write operation, </span><span class="cls_007">version</span><span class="cls_006"> is the old</span></div>
<div style="position:absolute;left:85.27px;top:207.23px" class="cls_006"><span class="cls_006">version number, and </span><span class="cls_007">total reads</span><span class="cls_006"> is the number of</span></div>
<div style="position:absolute;left:85.27px;top:236.23px" class="cls_006"><span class="cls_006">read operations that read the old version of the</span></div>
<div style="position:absolute;left:85.27px;top:265.23px" class="cls_006"><span class="cls_006">variable.</span></div>
<div style="position:absolute;left:83.27px;top:543.60px" class="cls_002"><span class="cls_002">Advanced Topics in Software Engineering</span></div>
<div style="position:absolute;left:659.27px;top:543.60px" class="cls_002"><span class="cls_002">10</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:6220px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="TracingReplayforSharedVariables/background11.jpg" width=792 height=612></div>
<div style="position:absolute;left:85.27px;top:60.08px" class="cls_016"><span class="cls_016">Encoding (cont</span><span class="cls_017">’</span><span class="cls_016">d)</span></div>
<div style="position:absolute;left:85.27px;top:121.23px" class="cls_006"><span class="cls_006">There are two important points to be made about</span></div>
<div style="position:absolute;left:85.27px;top:149.23px" class="cls_006"><span class="cls_006">the definition of RW-sequence:</span></div>
<div style="position:absolute;left:85.27px;top:192.63px" class="cls_004"><span class="cls_004">q</span><span class="cls_005"> </span><span class="cls_006">We record the </span><span class="cls_007">order</span><span class="cls_006"> in which read and write</span></div>
<div style="position:absolute;left:85.27px;top:221.23px" class="cls_006"><span class="cls_006">operations are performed, not the </span><span class="cls_007">values</span><span class="cls_006"> that are</span></div>
<div style="position:absolute;left:85.27px;top:250.23px" class="cls_006"><span class="cls_006">read and written.</span></div>
<div style="position:absolute;left:85.27px;top:293.63px" class="cls_004"><span class="cls_004">q</span><span class="cls_005"> </span><span class="cls_006">There is no need to record any </span><span class="cls_007">type</span><span class="cls_006"> information.</span></div>
<div style="position:absolute;left:83.53px;top:543.60px" class="cls_002"><span class="cls_002">Advanced Topics in Software Engineering</span></div>
<div style="position:absolute;left:659.53px;top:543.60px" class="cls_002"><span class="cls_002">11</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:6842px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="TracingReplayforSharedVariables/background12.jpg" width=792 height=612></div>
<div style="position:absolute;left:85.27px;top:60.08px" class="cls_016"><span class="cls_016">Example</span></div>
<div style="position:absolute;left:85.27px;top:121.23px" class="cls_018"><span class="cls_018">T1</span></div>
<div style="position:absolute;left:265.90px;top:121.23px" class="cls_018"><span class="cls_018">T2</span></div>
<div style="position:absolute;left:481.90px;top:121.23px" class="cls_018"><span class="cls_018">T3</span></div>
<div style="position:absolute;left:85.27px;top:163.63px" class="cls_006"><span class="cls_006">temp = s;</span></div>
<div style="position:absolute;left:265.90px;top:163.63px" class="cls_006"><span class="cls_006">temp = s;</span></div>
<div style="position:absolute;left:481.90px;top:163.63px" class="cls_006"><span class="cls_006">s = s + 1;</span></div>
<div style="position:absolute;left:85.27px;top:207.63px" class="cls_006"><span class="cls_006">Assume that s is initialized to 0. A possible RW-</span></div>
<div style="position:absolute;left:85.27px;top:236.23px" class="cls_006"><span class="cls_006">sequence of s is:</span></div>
<div style="position:absolute;left:295.27px;top:303.60px" class="cls_009"><span class="cls_009">(3, 0)</span></div>
<div style="position:absolute;left:295.27px;top:339.60px" class="cls_009"><span class="cls_009">(1, 0)</span></div>
<div style="position:absolute;left:295.27px;top:375.60px" class="cls_009"><span class="cls_009">(3, 0, 2)</span></div>
<div style="position:absolute;left:295.27px;top:411.60px" class="cls_009"><span class="cls_009">(2, 1)</span></div>
<div style="position:absolute;left:83.27px;top:543.60px" class="cls_002"><span class="cls_002">Advanced Topics in Software Engineering</span></div>
<div style="position:absolute;left:659.27px;top:543.60px" class="cls_002"><span class="cls_002">12</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:7464px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="TracingReplayforSharedVariables/background13.jpg" width=792 height=612></div>
<div style="position:absolute;left:85.27px;top:60.08px" class="cls_016"><span class="cls_016">Tracing/Replay for Shared Variables</span></div>
<div style="position:absolute;left:85.27px;top:121.23px" class="cls_004"><span class="cls_004">q</span><span class="cls_005"> </span><span class="cls_006">Introduction</span></div>
<div style="position:absolute;left:85.27px;top:163.63px" class="cls_004"><span class="cls_004">q</span><span class="cls_005"> </span><span class="cls_006">RW-sequence</span></div>
<div style="position:absolute;left:85.27px;top:207.63px" class="cls_004"><span class="cls_004">q</span><span class="cls_005"> </span><span class="cls_007">Tracing and Replay</span></div>
<div style="position:absolute;left:85.27px;top:250.63px" class="cls_004"><span class="cls_004">q</span><span class="cls_005"> </span><span class="cls_006">Implementation Issues</span></div>
<div style="position:absolute;left:83.27px;top:543.60px" class="cls_002"><span class="cls_002">Advanced Topics in Software Engineering</span></div>
<div style="position:absolute;left:659.27px;top:543.60px" class="cls_002"><span class="cls_002">13</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:8086px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="TracingReplayforSharedVariables/background14.jpg" width=792 height=612></div>
<div style="position:absolute;left:85.27px;top:60.08px" class="cls_016"><span class="cls_016">Tracing and Replay</span></div>
<div style="position:absolute;left:85.27px;top:121.23px" class="cls_006"><span class="cls_006">During program tracing, a </span><span class="cls_007">RW-sequence</span><span class="cls_006"> is recorded</span></div>
<div style="position:absolute;left:85.27px;top:149.23px" class="cls_006"><span class="cls_006">for each shared variable.</span></div>
<div style="position:absolute;left:85.27px;top:192.63px" class="cls_006"><span class="cls_006">During program replay, we make sure that each read/</span></div>
<div style="position:absolute;left:85.27px;top:221.23px" class="cls_006"><span class="cls_006">write operation reads/writes the same version of</span></div>
<div style="position:absolute;left:85.27px;top:250.23px" class="cls_006"><span class="cls_006">shared variable as recorded in a RW-sequence.</span></div>
<div style="position:absolute;left:83.27px;top:543.60px" class="cls_002"><span class="cls_002">Advanced Topics in Software Engineering</span></div>
<div style="position:absolute;left:659.27px;top:543.60px" class="cls_002"><span class="cls_002">14</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:8708px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="TracingReplayforSharedVariables/background15.jpg" width=792 height=612></div>
<div style="position:absolute;left:85.27px;top:60.08px" class="cls_016"><span class="cls_016">Instrumentation</span></div>
<div style="position:absolute;left:85.27px;top:121.23px" class="cls_006"><span class="cls_006">Read and write operations are instrumented by</span></div>
<div style="position:absolute;left:85.27px;top:149.23px" class="cls_006"><span class="cls_006">adding routines to control the start and end of the</span></div>
<div style="position:absolute;left:85.27px;top:178.23px" class="cls_006"><span class="cls_006">operation:</span></div>
<div style="position:absolute;left:130.77px;top:237.60px" class="cls_010"><span class="cls_010">read</span></div>
<div style="position:absolute;left:437.88px;top:237.60px" class="cls_010"><span class="cls_010">write</span></div>
<div style="position:absolute;left:121.27px;top:267.60px" class="cls_009"><span class="cls_009">startRead(x)</span></div>
<div style="position:absolute;left:433.27px;top:267.60px" class="cls_009"><span class="cls_009">startWrite(x</span><span class="cls_012">)</span></div>
<div style="position:absolute;left:121.27px;top:291.60px" class="cls_011"><span class="cls_011">read the value of x</span></div>
<div style="position:absolute;left:433.27px;top:291.60px" class="cls_011"><span class="cls_011">write the value of x</span></div>
<div style="position:absolute;left:121.27px;top:315.60px" class="cls_009"><span class="cls_009">endRead(x)</span></div>
<div style="position:absolute;left:433.27px;top:315.60px" class="cls_009"><span class="cls_009">endWrite(x)</span></div>
<div style="position:absolute;left:83.27px;top:543.60px" class="cls_002"><span class="cls_002">Advanced Topics in Software Engineering</span></div>
<div style="position:absolute;left:659.27px;top:543.60px" class="cls_002"><span class="cls_002">15</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:9330px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="TracingReplayforSharedVariables/background16.jpg" width=792 height=612></div>
<div style="position:absolute;left:85.27px;top:60.08px" class="cls_016"><span class="cls_016">startRead & endRead</span></div>
<div style="position:absolute;left:85.27px;top:119.43px" class="cls_019"><span class="cls_019">startRead (x)</span></div>
<div style="position:absolute;left:121.90px;top:149.23px" class="cls_013"><span class="cls_013">if</span><span class="cls_014"> (mode == </span><span class="cls_015">trace</span><span class="cls_014">) {</span></div>
<div style="position:absolute;left:157.27px;top:168.43px" class="cls_015"><span class="cls_015">RWSeq</span><span class="cls_014">.record(ID, x.version);</span></div>
<div style="position:absolute;left:121.90px;top:188.43px" class="cls_014"><span class="cls_014">}</span></div>
<div style="position:absolute;left:121.90px;top:207.43px" class="cls_013"><span class="cls_013">else</span><span class="cls_014"> {</span></div>
<div style="position:absolute;left:157.27px;top:227.43px" class="cls_014"><span class="cls_014">readEvent r = </span><span class="cls_015">RWSeq</span><span class="cls_014">.nextEvent (ID);</span></div>
<div style="position:absolute;left:157.27px;top:246.43px" class="cls_014"><span class="cls_014">myVersion = r.getVersion ();</span></div>
<div style="position:absolute;left:157.27px;top:265.43px" class="cls_013"><span class="cls_013">while</span><span class="cls_014"> (myVersion != x.version) delay;</span></div>
<div style="position:absolute;left:121.90px;top:285.43px" class="cls_014"><span class="cls_014">}</span></div>
<div style="position:absolute;left:85.27px;top:385.80px" class="cls_019"><span class="cls_019">endRead (x)</span></div>
<div style="position:absolute;left:121.90px;top:415.60px" class="cls_014"><span class="cls_014">++ x.totalReaders;</span></div>
<div style="position:absolute;left:83.27px;top:543.60px" class="cls_002"><span class="cls_002">Advanced Topics in Software Engineering</span></div>
<div style="position:absolute;left:659.27px;top:543.60px" class="cls_002"><span class="cls_002">16</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:9952px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="TracingReplayforSharedVariables/background17.jpg" width=792 height=612></div>
<div style="position:absolute;left:85.27px;top:60.08px" class="cls_016"><span class="cls_016">startWrite & endWrite</span></div>
<div style="position:absolute;left:85.27px;top:119.43px" class="cls_019"><span class="cls_019">startWrite (x)</span></div>
<div style="position:absolute;left:121.90px;top:149.23px" class="cls_013"><span class="cls_013">if</span><span class="cls_014"> (mode == </span><span class="cls_015">trace</span><span class="cls_014">) {</span></div>
<div style="position:absolute;left:157.27px;top:168.43px" class="cls_015"><span class="cls_015">RWSeq</span><span class="cls_014">.record(ID, x.version, x.totalReaders);</span></div>
<div style="position:absolute;left:121.90px;top:188.43px" class="cls_014"><span class="cls_014">}</span></div>
<div style="position:absolute;left:121.90px;top:207.43px" class="cls_013"><span class="cls_013">else</span><span class="cls_014"> {</span></div>
<div style="position:absolute;left:157.27px;top:227.43px" class="cls_014"><span class="cls_014">writeEvent w = </span><span class="cls_015">RWSeq</span><span class="cls_014">.nextEvent (ID);</span></div>
<div style="position:absolute;left:157.27px;top:246.43px" class="cls_014"><span class="cls_014">myVersion = w.getVersion ();</span></div>
<div style="position:absolute;left:157.27px;top:265.43px" class="cls_013"><span class="cls_013">while</span><span class="cls_014"> (myVersion != x.version) delay;</span></div>
<div style="position:absolute;left:157.27px;top:285.43px" class="cls_014"><span class="cls_014">myTotalReaders = w.getTotalReaders ();</span></div>
<div style="position:absolute;left:157.27px;top:304.43px" class="cls_014"><span class="cls_014">while (x.totalReaders &lt; myTotalReaders) delay;</span></div>
<div style="position:absolute;left:121.90px;top:324.43px" class="cls_014"><span class="cls_014">}</span></div>
<div style="position:absolute;left:85.27px;top:415.80px" class="cls_019"><span class="cls_019">endWrite (x)</span></div>
<div style="position:absolute;left:121.90px;top:445.60px" class="cls_014"><span class="cls_014">x.totalReaders == 0;</span></div>
<div style="position:absolute;left:121.90px;top:464.80px" class="cls_014"><span class="cls_014">++ x.version;</span></div>
<div style="position:absolute;left:83.27px;top:543.60px" class="cls_002"><span class="cls_002">Advanced Topics in Software Engineering</span></div>
<div style="position:absolute;left:659.27px;top:543.60px" class="cls_002"><span class="cls_002">17</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:10574px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="TracingReplayforSharedVariables/background18.jpg" width=792 height=612></div>
<div style="position:absolute;left:85.27px;top:60.08px" class="cls_016"><span class="cls_016">CREW</span></div>
<div style="position:absolute;left:85.27px;top:121.23px" class="cls_006"><span class="cls_006">If two or more readers read a particular version</span></div>
<div style="position:absolute;left:85.27px;top:149.23px" class="cls_006"><span class="cls_006">during tracing, then they must </span><span class="cls_007">read</span><span class="cls_006"> the same version</span></div>
<div style="position:absolute;left:85.27px;top:178.23px" class="cls_006"><span class="cls_006">during replay, but these </span><span class="cls_007">read</span><span class="cls_006"> operations can be in</span></div>
<div style="position:absolute;left:85.27px;top:207.23px" class="cls_006"><span class="cls_006">any order.</span></div>
<div style="position:absolute;left:85.27px;top:250.63px" class="cls_006"><span class="cls_006">In our solution, readers are only </span><span class="cls_007">partially ordered</span></div>
<div style="position:absolute;left:85.27px;top:279.23px" class="cls_006"><span class="cls_006">with respect to write operations, while write</span></div>
<div style="position:absolute;left:85.27px;top:308.23px" class="cls_006"><span class="cls_006">operations are </span><span class="cls_007">totally ordered</span><span class="cls_006"> for each shared</span></div>
<div style="position:absolute;left:85.27px;top:337.23px" class="cls_006"><span class="cls_006">variable.</span></div>
<div style="position:absolute;left:85.27px;top:379.63px" class="cls_006"><span class="cls_006">This is known as </span><span class="cls_007">Concurrent Reading and Exclusive</span></div>
<div style="position:absolute;left:85.27px;top:409.23px" class="cls_007"><span class="cls_007">Writing (CREW)</span><span class="cls_006"> policy.</span></div>
<div style="position:absolute;left:83.27px;top:543.60px" class="cls_002"><span class="cls_002">Advanced Topics in Software Engineering</span></div>
<div style="position:absolute;left:659.27px;top:543.60px" class="cls_002"><span class="cls_002">18</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:11196px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="TracingReplayforSharedVariables/background19.jpg" width=792 height=612></div>
<div style="position:absolute;left:85.27px;top:60.08px" class="cls_016"><span class="cls_016">C++ Implementation</span></div>
<div style="position:absolute;left:85.27px;top:121.23px" class="cls_006"><span class="cls_006">The textbook has shown a template class</span></div>
<div style="position:absolute;left:85.27px;top:149.23px" class="cls_007"><span class="cls_007">sharedVariable</span><span class="cls_006"> that can be used to trace and replay</span></div>
<div style="position:absolute;left:85.27px;top:178.23px" class="cls_006"><span class="cls_006">C++ programs.</span></div>
<div style="position:absolute;left:85.27px;top:221.63px" class="cls_006"><span class="cls_006">The only change involved is to wrap each variable as</span></div>
<div style="position:absolute;left:85.27px;top:250.23px" class="cls_006"><span class="cls_006">an instance of </span><span class="cls_007">sharedVariable</span><span class="cls_006">.</span></div>
<div style="position:absolute;left:109.27px;top:327.60px" class="cls_010"><span class="cls_010">int</span><span class="cls_009"> </span><span class="cls_011">turn</span><span class="cls_009">;</span></div>
<div style="position:absolute;left:373.27px;top:327.60px" class="cls_009"><span class="cls_009">sharedVariable</span><span class="cls_011">&lt;</span><span class="cls_010">int</span><span class="cls_011">></span><span class="cls_009"> </span><span class="cls_011">turn</span><span class="cls_009">;</span></div>
<div style="position:absolute;left:83.27px;top:543.60px" class="cls_002"><span class="cls_002">Advanced Topics in Software Engineering</span></div>
<div style="position:absolute;left:659.27px;top:543.60px" class="cls_002"><span class="cls_002">19</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:11818px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="TracingReplayforSharedVariables/background20.jpg" width=792 height=612></div>
<div style="position:absolute;left:85.27px;top:60.08px" class="cls_016"><span class="cls_016">Java Implementation</span></div>
<div style="position:absolute;left:85.27px;top:121.23px" class="cls_006"><span class="cls_006">More changes are necessary for Java programs, due</span></div>
<div style="position:absolute;left:85.27px;top:149.23px" class="cls_006"><span class="cls_006">to the lack of the features of template class and</span></div>
<div style="position:absolute;left:85.27px;top:178.23px" class="cls_006"><span class="cls_006">operator overloading.</span></div>
<div style="position:absolute;left:85.27px;top:221.63px" class="cls_006"><span class="cls_006">However, such changes can be made automatically,</span></div>
<div style="position:absolute;left:85.27px;top:250.23px" class="cls_006"><span class="cls_006">without leaving developers with too much burden.</span></div>
<div style="position:absolute;left:83.27px;top:543.60px" class="cls_002"><span class="cls_002">Advanced Topics in Software Engineering</span></div>
<div style="position:absolute;left:659.27px;top:543.60px" class="cls_002"><span class="cls_002">20</span></div>
</div>

</body>
</html>
