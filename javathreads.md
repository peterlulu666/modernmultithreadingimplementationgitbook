<html>
<head><meta http-equiv=Content-Type content="text/html; charset=UTF-8">
<style type="text/css">
<!--
span.cls_019{font-family:"Comic Sans MS",serif;font-size:28.1px;color:rgb(51,51,204);font-weight:normal;font-style:normal;text-decoration: underline}
div.cls_019{font-family:"Comic Sans MS",serif;font-size:28.1px;color:rgb(51,51,204);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_004{font-family:Arial,serif;font-size:20.4px;color:rgb(51,51,204);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_004{font-family:Arial,serif;font-size:20.4px;color:rgb(51,51,204);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_005{font-family:Arial,serif;font-size:20.4px;color:rgb(66,77,213);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_005{font-family:Arial,serif;font-size:20.4px;color:rgb(66,77,213);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_007{font-family:"Comic Sans MS",serif;font-size:24.1px;color:rgb(0,127,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_007{font-family:"Comic Sans MS",serif;font-size:24.1px;color:rgb(0,127,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_006{font-family:"Comic Sans MS",serif;font-size:24.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_006{font-family:"Comic Sans MS",serif;font-size:24.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_002{font-family:Times,serif;font-size:14.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_002{font-family:Times,serif;font-size:14.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_008{font-family:"Comic Sans MS",serif;font-size:16.1px;color:rgb(51,51,204);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_008{font-family:"Comic Sans MS",serif;font-size:16.1px;color:rgb(51,51,204);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_009{font-family:"Comic Sans MS",serif;font-size:16.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_009{font-family:"Comic Sans MS",serif;font-size:16.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_010{font-family:"Comic Sans MS",serif;font-size:16.1px;color:rgb(0,127,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_010{font-family:"Comic Sans MS",serif;font-size:16.1px;color:rgb(0,127,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_011{font-family:Arial,serif;font-size:16.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_011{font-family:Arial,serif;font-size:16.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_012{font-family:"Comic Sans MS",serif;font-size:20.1px;color:rgb(204,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_012{font-family:"Comic Sans MS",serif;font-size:20.1px;color:rgb(204,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_013{font-family:"Comic Sans MS",serif;font-size:12.1px;color:rgb(204,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_013{font-family:"Comic Sans MS",serif;font-size:12.1px;color:rgb(204,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_014{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_014{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_015{font-family:"Comic Sans MS",serif;font-size:24.1px;color:rgb(51,51,204);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_015{font-family:"Comic Sans MS",serif;font-size:24.1px;color:rgb(51,51,204);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_020{font-family:"Comic Sans MS",serif;font-size:24.1px;color:rgb(204,203,255);font-weight:normal;font-style:normal;text-decoration: underline}
div.cls_020{font-family:"Comic Sans MS",serif;font-size:24.1px;color:rgb(204,203,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_017{font-family:"Comic Sans MS",serif;font-size:10.1px;color:rgb(51,51,204);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_017{font-family:"Comic Sans MS",serif;font-size:10.1px;color:rgb(51,51,204);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_018{font-family:"Comic Sans MS",serif;font-size:10.0px;color:rgb(51,51,204);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_018{font-family:"Comic Sans MS",serif;font-size:10.0px;color:rgb(51,51,204);font-weight:normal;font-style:normal;text-decoration: none}
-->
</style>
<script type="text/javascript" src="javathreads/wz_jsgraphics.js"></script>
</head>
<body>
<div style="position:absolute;left:50%;margin-left:-396px;top:0px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="javathreads/background01.jpg" width=792 height=612></div>
<div style="position:absolute;left:85.27px;top:60.08px" class="cls_019"><span class="cls_019">Java Threads</span></div>
<div style="position:absolute;left:85.27px;top:121.23px" class="cls_004"><span class="cls_004">q</span><span class="cls_005"> </span><span class="cls_007">Thread Creation</span></div>
<div style="position:absolute;left:85.27px;top:163.63px" class="cls_004"><span class="cls_004">q</span><span class="cls_005"> </span><span class="cls_006">Thread Synchronization</span></div>
<div style="position:absolute;left:85.27px;top:207.63px" class="cls_004"><span class="cls_004">q</span><span class="cls_005"> </span><span class="cls_006">Thread States And Scheduling</span></div>
<div style="position:absolute;left:85.27px;top:250.63px" class="cls_004"><span class="cls_004">q</span><span class="cls_005"> </span><span class="cls_006">Short Demo</span></div>
<div style="position:absolute;left:86.77px;top:543.60px" class="cls_002"><span class="cls_002">Advanced Topics in Software Engineering</span></div>
<div style="position:absolute;left:662.77px;top:543.60px" class="cls_002"><span class="cls_002">1</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:622px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="javathreads/background02.jpg" width=792 height=612></div>
<div style="position:absolute;left:85.27px;top:60.08px" class="cls_019"><span class="cls_019">Thread Creation</span></div>
<div style="position:absolute;left:85.27px;top:121.23px" class="cls_006"><span class="cls_006">There are two ways to create a thread in Java:</span></div>
<div style="position:absolute;left:85.27px;top:163.63px" class="cls_004"><span class="cls_004">q</span><span class="cls_005"> </span><span class="cls_006">Extend the </span><span class="cls_007">Thread</span><span class="cls_006"> class</span></div>
<div style="position:absolute;left:85.27px;top:207.63px" class="cls_004"><span class="cls_004">q</span><span class="cls_005"> </span><span class="cls_006">Implement the </span><span class="cls_007">Runnable</span><span class="cls_006"> interface</span></div>
<div style="position:absolute;left:86.77px;top:543.60px" class="cls_002"><span class="cls_002">Advanced Topics in Software Engineering</span></div>
<div style="position:absolute;left:662.77px;top:543.60px" class="cls_002"><span class="cls_002">2</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:1244px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="javathreads/background03.jpg" width=792 height=612></div>
<div style="position:absolute;left:85.27px;top:60.08px" class="cls_019"><span class="cls_019">The Thread class</span></div>
<div style="position:absolute;left:85.27px;top:121.23px" class="cls_008"><span class="cls_008">class</span><span class="cls_009"> </span><span class="cls_010">A</span><span class="cls_009"> </span><span class="cls_008">extends</span><span class="cls_009"> </span><span class="cls_010">Thread</span><span class="cls_009"> {</span></div>
<div style="position:absolute;left:121.77px;top:140.23px" class="cls_008"><span class="cls_008">public</span><span class="cls_009"> A (</span><span class="cls_010">String</span><span class="cls_009"> name) { </span><span class="cls_008">super</span><span class="cls_009"> (name); }</span></div>
<div style="position:absolute;left:121.77px;top:159.23px" class="cls_008"><span class="cls_008">public</span><span class="cls_009"> </span><span class="cls_008">void</span><span class="cls_009"> run () {</span></div>
<div style="position:absolute;left:157.27px;top:178.23px" class="cls_009"><span class="cls_009">System.out.println(</span><span class="cls_011">“</span><span class="cls_009">My name is </span><span class="cls_011">”</span><span class="cls_009"> + getName());</span></div>
<div style="position:absolute;left:121.77px;top:197.23px" class="cls_009"><span class="cls_009">}</span></div>
<div style="position:absolute;left:85.27px;top:217.23px" class="cls_009"><span class="cls_009">}</span></div>
<div style="position:absolute;left:85.27px;top:236.23px" class="cls_008"><span class="cls_008">public class</span><span class="cls_009"> </span><span class="cls_010">B</span><span class="cls_009"> {</span></div>
<div style="position:absolute;left:121.77px;top:255.23px" class="cls_008"><span class="cls_008">public</span><span class="cls_009"> </span><span class="cls_008">static</span><span class="cls_009"> </span><span class="cls_008">void</span><span class="cls_009"> main (</span><span class="cls_010">String</span><span class="cls_009"> [] args) {</span></div>
<div style="position:absolute;left:157.27px;top:274.23px" class="cls_010"><span class="cls_010">A</span><span class="cls_009"> a = </span><span class="cls_008">new</span><span class="cls_009"> </span><span class="cls_010">A</span><span class="cls_009"> (</span><span class="cls_011">“</span><span class="cls_009">mud</span><span class="cls_011">”</span><span class="cls_009">);</span></div>
<div style="position:absolute;left:157.27px;top:293.23px" class="cls_009"><span class="cls_009">a.start ();</span></div>
<div style="position:absolute;left:121.77px;top:312.23px" class="cls_009"><span class="cls_009">}</span></div>
<div style="position:absolute;left:85.27px;top:332.23px" class="cls_009"><span class="cls_009">}</span></div>
<div style="position:absolute;left:86.77px;top:543.60px" class="cls_002"><span class="cls_002">Advanced Topics in Software Engineering</span></div>
<div style="position:absolute;left:662.77px;top:543.60px" class="cls_002"><span class="cls_002">3</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:1866px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="javathreads/background04.jpg" width=792 height=612></div>
<div style="position:absolute;left:85.27px;top:60.08px" class="cls_019"><span class="cls_019">The Runnable interface</span></div>
<div style="position:absolute;left:85.27px;top:121.23px" class="cls_008"><span class="cls_008">class</span><span class="cls_009"> </span><span class="cls_010">A</span><span class="cls_009"> </span><span class="cls_008">extends</span><span class="cls_009"> ... </span><span class="cls_008">implements</span><span class="cls_009"> </span><span class="cls_010">Runnable</span><span class="cls_009"> {</span></div>
<div style="position:absolute;left:121.77px;top:140.23px" class="cls_008"><span class="cls_008">public</span><span class="cls_009"> </span><span class="cls_008">void</span><span class="cls_009"> run () {</span></div>
<div style="position:absolute;left:157.27px;top:159.23px" class="cls_009"><span class="cls_009">System.out.println(</span><span class="cls_011">“</span><span class="cls_009">My name is </span><span class="cls_011">”</span><span class="cls_009"> + getName () );</span></div>
<div style="position:absolute;left:121.77px;top:178.23px" class="cls_009"><span class="cls_009">}</span></div>
<div style="position:absolute;left:85.27px;top:197.23px" class="cls_009"><span class="cls_009">}</span></div>
<div style="position:absolute;left:85.27px;top:217.23px" class="cls_008"><span class="cls_008">public class</span><span class="cls_009"> </span><span class="cls_010">B</span><span class="cls_009"> {</span></div>
<div style="position:absolute;left:121.77px;top:236.23px" class="cls_008"><span class="cls_008">public</span><span class="cls_009"> </span><span class="cls_008">static</span><span class="cls_009"> </span><span class="cls_008">void</span><span class="cls_009"> main (</span><span class="cls_010">String</span><span class="cls_009"> [] args) {</span></div>
<div style="position:absolute;left:157.27px;top:255.23px" class="cls_010"><span class="cls_010">A</span><span class="cls_009"> a = </span><span class="cls_008">new</span><span class="cls_009"> </span><span class="cls_010">A</span><span class="cls_009"> ();</span></div>
<div style="position:absolute;left:157.27px;top:274.23px" class="cls_010"><span class="cls_010">Thread</span><span class="cls_009"> t = </span><span class="cls_008">new</span><span class="cls_009"> </span><span class="cls_010">Thread</span><span class="cls_009"> (a, </span><span class="cls_011">“</span><span class="cls_009">mud, too</span><span class="cls_011">”</span><span class="cls_009">);</span></div>
<div style="position:absolute;left:157.27px;top:293.23px" class="cls_009"><span class="cls_009">t.start ();</span></div>
<div style="position:absolute;left:121.77px;top:312.23px" class="cls_009"><span class="cls_009">}</span></div>
<div style="position:absolute;left:85.27px;top:324.23px" class="cls_009"><span class="cls_009">}</span></div>
<div style="position:absolute;left:86.77px;top:543.60px" class="cls_002"><span class="cls_002">Advanced Topics in Software Engineering</span></div>
<div style="position:absolute;left:662.77px;top:543.60px" class="cls_002"><span class="cls_002">4</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:2488px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="javathreads/background05.jpg" width=792 height=612></div>
<div style="position:absolute;left:85.27px;top:60.08px" class="cls_019"><span class="cls_019">Java Threads</span></div>
<div style="position:absolute;left:85.27px;top:121.23px" class="cls_004"><span class="cls_004">q</span><span class="cls_005"> </span><span class="cls_006">Thread Creation</span></div>
<div style="position:absolute;left:85.27px;top:163.63px" class="cls_004"><span class="cls_004">q</span><span class="cls_005"> </span><span class="cls_007">Thread Synchronization</span></div>
<div style="position:absolute;left:85.27px;top:207.63px" class="cls_004"><span class="cls_004">q</span><span class="cls_005"> </span><span class="cls_006">Thread States And Scheduling</span></div>
<div style="position:absolute;left:85.27px;top:250.63px" class="cls_004"><span class="cls_004">q</span><span class="cls_005"> </span><span class="cls_006">Short Demo</span></div>
<div style="position:absolute;left:86.77px;top:543.60px" class="cls_002"><span class="cls_002">Advanced Topics in Software Engineering</span></div>
<div style="position:absolute;left:662.77px;top:543.60px" class="cls_002"><span class="cls_002">5</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:3110px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="javathreads/background06.jpg" width=792 height=612></div>
<div style="position:absolute;left:85.27px;top:60.08px" class="cls_019"><span class="cls_019">Lock</span></div>
<div style="position:absolute;left:85.27px;top:121.23px" class="cls_006"><span class="cls_006">Each Java object is implicitly associated with a </span><span class="cls_007">lock</span><span class="cls_006">.</span></div>
<div style="position:absolute;left:85.27px;top:163.63px" class="cls_006"><span class="cls_006">To invoke a </span><span class="cls_007">synchronized</span><span class="cls_006"> method of an object, a</span></div>
<div style="position:absolute;left:85.27px;top:193.23px" class="cls_006"><span class="cls_006">thread must obtain the </span><span class="cls_007">lock</span><span class="cls_006"> associated with this</span></div>
<div style="position:absolute;left:85.27px;top:221.23px" class="cls_006"><span class="cls_006">object.</span></div>
<div style="position:absolute;left:85.27px;top:264.63px" class="cls_006"><span class="cls_006">The </span><span class="cls_007">lock</span><span class="cls_006"> is not released until the execution of the</span></div>
<div style="position:absolute;left:85.27px;top:293.23px" class="cls_006"><span class="cls_006">method completes.</span></div>
<div style="position:absolute;left:85.27px;top:336.63px" class="cls_006"><span class="cls_006">The locking mechanism ensures that at any given</span></div>
<div style="position:absolute;left:85.27px;top:365.23px" class="cls_006"><span class="cls_006">time, at most one thread can execute any</span></div>
<div style="position:absolute;left:85.27px;top:394.23px" class="cls_007"><span class="cls_007">synchronized</span><span class="cls_006"> method of an object.</span></div>
<div style="position:absolute;left:85.27px;top:437.63px" class="cls_007"><span class="cls_007">Important</span><span class="cls_006">: Lock is per object (NOT per method)!</span></div>
<div style="position:absolute;left:86.77px;top:543.60px" class="cls_002"><span class="cls_002">Advanced Topics in Software Engineering</span></div>
<div style="position:absolute;left:662.77px;top:543.60px" class="cls_002"><span class="cls_002">6</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:3732px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="javathreads/background07.jpg" width=792 height=612></div>
<div style="position:absolute;left:85.27px;top:60.08px" class="cls_019"><span class="cls_019">wait, notify and notifyAll</span></div>
<div style="position:absolute;left:85.27px;top:121.23px" class="cls_006"><span class="cls_006">The execution of </span><span class="cls_007">wait</span><span class="cls_006"> on an object causes the</span></div>
<div style="position:absolute;left:85.27px;top:149.23px" class="cls_006"><span class="cls_006">current thread to wait until some other thread to</span></div>
<div style="position:absolute;left:85.27px;top:178.23px" class="cls_006"><span class="cls_006">call </span><span class="cls_007">notify</span><span class="cls_006"> or </span><span class="cls_007">notifyAll</span><span class="cls_006">.</span></div>
<div style="position:absolute;left:85.27px;top:221.63px" class="cls_006"><span class="cls_006">A thread must own the object </span><span class="cls_007">lock</span><span class="cls_006"> before it invokes</span></div>
<div style="position:absolute;left:85.27px;top:250.23px" class="cls_007"><span class="cls_007">wait</span><span class="cls_006"> on an object. The execution of </span><span class="cls_007">wait</span><span class="cls_006"> will also</span></div>
<div style="position:absolute;left:85.27px;top:279.23px" class="cls_006"><span class="cls_006">release the lock.</span></div>
<div style="position:absolute;left:85.27px;top:322.63px" class="cls_006"><span class="cls_006">When a waiting thread is notified, it has to compete</span></div>
<div style="position:absolute;left:85.27px;top:351.23px" class="cls_006"><span class="cls_006">and reacquire the object lock before it continues</span></div>
<div style="position:absolute;left:85.27px;top:380.23px" class="cls_006"><span class="cls_006">execution.</span></div>
<div style="position:absolute;left:85.27px;top:423.63px" class="cls_006"><span class="cls_006">Note that </span><span class="cls_007">notify</span><span class="cls_006"> wakes up one waiting thread, and</span></div>
<div style="position:absolute;left:85.27px;top:452.23px" class="cls_007"><span class="cls_007">notifyAll </span><span class="cls_006">wakes up all the waiting threads.</span></div>
<div style="position:absolute;left:86.77px;top:543.60px" class="cls_002"><span class="cls_002">Advanced Topics in Software Engineering</span></div>
<div style="position:absolute;left:662.77px;top:543.60px" class="cls_002"><span class="cls_002">7</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:4354px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="javathreads/background08.jpg" width=792 height=612></div>
<div style="position:absolute;left:85.27px;top:60.08px" class="cls_019"><span class="cls_019">join</span></div>
<div style="position:absolute;left:85.27px;top:121.23px" class="cls_006"><span class="cls_006">A thread </span><span class="cls_007">t1</span><span class="cls_006"> can wait until another thread </span><span class="cls_007">t2</span><span class="cls_006"> to</span></div>
<div style="position:absolute;left:85.27px;top:149.23px" class="cls_006"><span class="cls_006">terminate.</span></div>
<div style="position:absolute;left:242.90px;top:238.98px" class="cls_012"><span class="cls_012">t1</span></div>
<div style="position:absolute;left:395.65px;top:237.60px" class="cls_012"><span class="cls_012">t2</span></div>
<div style="position:absolute;left:229.27px;top:327.60px" class="cls_013"><span class="cls_013">t2.join ()</span></div>
<div style="position:absolute;left:391.27px;top:369.60px" class="cls_013"><span class="cls_013">end</span></div>
<div style="position:absolute;left:86.77px;top:543.60px" class="cls_002"><span class="cls_002">Advanced Topics in Software Engineering</span></div>
<div style="position:absolute;left:662.77px;top:543.60px" class="cls_002"><span class="cls_002">8</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:4976px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="javathreads/background09.jpg" width=792 height=612></div>
<div style="position:absolute;left:85.27px;top:60.08px" class="cls_019"><span class="cls_019">interrupt</span></div>
<div style="position:absolute;left:85.27px;top:121.23px" class="cls_007"><span class="cls_007">interrupt</span><span class="cls_006"> allows one thread to send a signal to</span></div>
<div style="position:absolute;left:85.27px;top:149.23px" class="cls_006"><span class="cls_006">another thread.</span></div>
<div style="position:absolute;left:85.27px;top:192.63px" class="cls_006"><span class="cls_006">It will set the thread</span><span class="cls_014">’</span><span class="cls_006">s interrupt status </span><span class="cls_007">flag</span><span class="cls_006">, and</span></div>
<div style="position:absolute;left:85.27px;top:221.23px" class="cls_006"><span class="cls_006">will throw a </span><span class="cls_007">ThreadInterrupted</span><span class="cls_006"> exception if</span></div>
<div style="position:absolute;left:85.27px;top:250.23px" class="cls_006"><span class="cls_006">necessary .</span></div>
<div style="position:absolute;left:85.27px;top:293.63px" class="cls_006"><span class="cls_006">The receiver thread can check the status </span><span class="cls_007">flag</span><span class="cls_006"> or</span></div>
<div style="position:absolute;left:85.27px;top:322.23px" class="cls_006"><span class="cls_006">catch the </span><span class="cls_007">exception,</span><span class="cls_006"> and then take appropriate</span></div>
<div style="position:absolute;left:85.27px;top:351.23px" class="cls_006"><span class="cls_006">actions.</span></div>
<div style="position:absolute;left:86.77px;top:543.60px" class="cls_002"><span class="cls_002">Advanced Topics in Software Engineering</span></div>
<div style="position:absolute;left:662.77px;top:543.60px" class="cls_002"><span class="cls_002">9</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:5598px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="javathreads/background10.jpg" width=792 height=612></div>
<div style="position:absolute;left:85.27px;top:60.08px" class="cls_019"><span class="cls_019">Other Thread methods</span></div>
<div style="position:absolute;left:85.27px;top:121.23px" class="cls_006"><span class="cls_006">Method</span><span class="cls_015"> </span><span class="cls_007">sleep</span><span class="cls_006"> puts the running thread into sleep,</span></div>
<div style="position:absolute;left:85.27px;top:149.23px" class="cls_006"><span class="cls_006">releasing the CPU.</span></div>
<div style="position:absolute;left:85.27px;top:192.63px" class="cls_006"><span class="cls_006">Method </span><span class="cls_007">suspend</span><span class="cls_006"> suspends the execution of a thread,</span></div>
<div style="position:absolute;left:85.27px;top:221.23px" class="cls_006"><span class="cls_006">which can be resumed later by another thread using</span></div>
<div style="position:absolute;left:85.27px;top:250.23px" class="cls_006"><span class="cls_006">method </span><span class="cls_007">resume</span><span class="cls_006">.</span></div>
<div style="position:absolute;left:85.27px;top:293.63px" class="cls_006"><span class="cls_006">Method </span><span class="cls_007">stop </span><span class="cls_006">ends the execution of a thread</span><span class="cls_007">.</span></div>
<div style="position:absolute;left:85.27px;top:336.63px" class="cls_006"><span class="cls_006">Note that </span><span class="cls_007">suspend</span><span class="cls_006">, </span><span class="cls_007">resume</span><span class="cls_006">, and </span><span class="cls_007">stop</span><span class="cls_006"> has been</span></div>
<div style="position:absolute;left:85.27px;top:365.23px" class="cls_006"><span class="cls_006">deprecated. More information can be found at</span></div>
<div style="position:absolute;left:85.27px;top:394.23px" class="cls_020"><span class="cls_020">Java Thread Primitive Deprecation</span><span class="cls_006">.</span></div>
<div style="position:absolute;left:83.27px;top:543.60px" class="cls_002"><span class="cls_002">Advanced Topics in Software Engineering</span></div>
<div style="position:absolute;left:659.27px;top:543.60px" class="cls_002"><span class="cls_002">10</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:6220px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="javathreads/background11.jpg" width=792 height=612></div>
<div style="position:absolute;left:85.27px;top:60.08px" class="cls_019"><span class="cls_019">Daemon Thread</span></div>
<div style="position:absolute;left:85.27px;top:127.23px" class="cls_006"><span class="cls_006">A </span><span class="cls_007">daemon</span><span class="cls_006"> thread is used to perform some services</span></div>
<div style="position:absolute;left:85.27px;top:155.23px" class="cls_006"><span class="cls_006">(e.g. cleanup) for other threads.</span></div>
<div style="position:absolute;left:85.27px;top:198.63px" class="cls_006"><span class="cls_006">Any thread can be marked as a daemon thread using</span></div>
<div style="position:absolute;left:85.27px;top:227.23px" class="cls_007"><span class="cls_007">setDaemon</span><span class="cls_006"> (</span><span class="cls_015">true</span><span class="cls_006">).</span></div>
<div style="position:absolute;left:85.27px;top:270.63px" class="cls_006"><span class="cls_006">A program terminates when all its </span><span class="cls_007">non-daemon</span></div>
<div style="position:absolute;left:85.27px;top:299.23px" class="cls_006"><span class="cls_006">threads terminate, meaning that </span><span class="cls_007">daemon</span><span class="cls_006"> threads die</span></div>
<div style="position:absolute;left:85.27px;top:328.23px" class="cls_006"><span class="cls_006">when all </span><span class="cls_007">non-daemon</span><span class="cls_006"> threads die.</span></div>
<div style="position:absolute;left:83.53px;top:543.60px" class="cls_002"><span class="cls_002">Advanced Topics in Software Engineering</span></div>
<div style="position:absolute;left:659.53px;top:543.60px" class="cls_002"><span class="cls_002">11</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:6842px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="javathreads/background12.jpg" width=792 height=612></div>
<div style="position:absolute;left:85.27px;top:60.08px" class="cls_019"><span class="cls_019">Java Threads</span></div>
<div style="position:absolute;left:85.27px;top:121.23px" class="cls_004"><span class="cls_004">q</span><span class="cls_005"> </span><span class="cls_006">Thread Creation</span></div>
<div style="position:absolute;left:85.27px;top:163.63px" class="cls_004"><span class="cls_004">q</span><span class="cls_005"> </span><span class="cls_006">Thread Synchronization</span></div>
<div style="position:absolute;left:85.27px;top:207.63px" class="cls_004"><span class="cls_004">q</span><span class="cls_005"> </span><span class="cls_007">Thread States And Scheduling</span></div>
<div style="position:absolute;left:85.27px;top:250.63px" class="cls_004"><span class="cls_004">q</span><span class="cls_005"> </span><span class="cls_006">Short Demo</span></div>
<div style="position:absolute;left:83.27px;top:543.60px" class="cls_002"><span class="cls_002">Advanced Topics in Software Engineering</span></div>
<div style="position:absolute;left:659.27px;top:543.60px" class="cls_002"><span class="cls_002">12</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:7464px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="javathreads/background13.jpg" width=792 height=612></div>
<div style="position:absolute;left:85.27px;top:60.08px" class="cls_019"><span class="cls_019">State Transition Diagram</span></div>
<div style="position:absolute;left:445.27px;top:111.60px" class="cls_017"><span class="cls_017">CS, yield</span></div>
<div style="position:absolute;left:223.27px;top:159.60px" class="cls_017"><span class="cls_017">start</span></div>
<div style="position:absolute;left:428.27px;top:159.60px" class="cls_017"><span class="cls_017">scheduled</span></div>
<div style="position:absolute;left:145.27px;top:165.60px" class="cls_013"><span class="cls_013">new</span></div>
<div style="position:absolute;left:324.15px;top:165.60px" class="cls_013"><span class="cls_013">runnable</span></div>
<div style="position:absolute;left:561.77px;top:165.60px" class="cls_013"><span class="cls_013">running</span></div>
<div style="position:absolute;left:355.27px;top:219.60px" class="cls_017"><span class="cls_017">IO completes,</span></div>
<div style="position:absolute;left:559.27px;top:225.60px" class="cls_017"><span class="cls_017">IO, sleep</span></div>
<div style="position:absolute;left:356.27px;top:231.60px" class="cls_017"><span class="cls_017">sleep expires,</span></div>
<div style="position:absolute;left:559.27px;top:237.60px" class="cls_017"><span class="cls_017">wait, join</span></div>
<div style="position:absolute;left:349.27px;top:243.60px" class="cls_017"><span class="cls_017">notify, notifyAll,</span></div>
<div style="position:absolute;left:355.27px;top:255.60px" class="cls_017"><span class="cls_017">join completes</span></div>
<div style="position:absolute;left:253.27px;top:273.60px" class="cls_013"><span class="cls_013">suspended</span></div>
<div style="position:absolute;left:464.65px;top:273.60px" class="cls_013"><span class="cls_013">blocked</span></div>
<div style="position:absolute;left:142.77px;top:375.60px" class="cls_013"><span class="cls_013">dead</span></div>
<div style="position:absolute;left:259.44px;top:375.20px" class="cls_018"><span class="cls_018">stop</span></div>
<div style="position:absolute;left:380.02px;top:375.60px" class="cls_013"><span class="cls_013">suspend</span></div>
<div style="position:absolute;left:381.02px;top:389.60px" class="cls_013"><span class="cls_013">blocked</span></div>
<div style="position:absolute;left:344.40px;top:441.60px" class="cls_017"><span class="cls_017">stop</span></div>
<div style="position:absolute;left:398.65px;top:483.60px" class="cls_017"><span class="cls_017">stop, run ends</span></div>
<div style="position:absolute;left:83.27px;top:543.60px" class="cls_002"><span class="cls_002">Advanced Topics in Software Engineering</span></div>
<div style="position:absolute;left:659.27px;top:543.60px" class="cls_002"><span class="cls_002">13</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:8086px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="javathreads/background14.jpg" width=792 height=612></div>
<div style="position:absolute;left:85.27px;top:60.08px" class="cls_019"><span class="cls_019">Scheduling</span></div>
<div style="position:absolute;left:85.27px;top:121.23px" class="cls_006"><span class="cls_006">In general, there are two types of scheduling: </span><span class="cls_007">non-</span></div>
<div style="position:absolute;left:85.27px;top:149.23px" class="cls_007"><span class="cls_007">preemptive scheduling</span><span class="cls_006">, and </span><span class="cls_007">preemptive scheduling</span><span class="cls_006">.</span></div>
<div style="position:absolute;left:85.27px;top:192.63px" class="cls_006"><span class="cls_006">In non-preemptive scheduling, a thread runs until it</span></div>
<div style="position:absolute;left:85.27px;top:221.23px" class="cls_006"><span class="cls_006">terminates, stops, blocks, suspends, or yields.</span></div>
<div style="position:absolute;left:85.27px;top:264.63px" class="cls_006"><span class="cls_006">In preemptive scheduling, even if the current thread</span></div>
<div style="position:absolute;left:85.27px;top:293.23px" class="cls_006"><span class="cls_006">is still running,  a context switch will occur when its</span></div>
<div style="position:absolute;left:85.27px;top:322.23px" class="cls_006"><span class="cls_006">time slice is used up.</span></div>
<div style="position:absolute;left:83.27px;top:543.60px" class="cls_002"><span class="cls_002">Advanced Topics in Software Engineering</span></div>
<div style="position:absolute;left:659.27px;top:543.60px" class="cls_002"><span class="cls_002">14</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:8708px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="javathreads/background15.jpg" width=792 height=612></div>
<div style="position:absolute;left:85.27px;top:60.08px" class="cls_019"><span class="cls_019">Priorities</span></div>
<div style="position:absolute;left:85.27px;top:121.23px" class="cls_006"><span class="cls_006">Each thread has a </span><span class="cls_007">priority</span><span class="cls_006"> that also affects their</span></div>
<div style="position:absolute;left:85.27px;top:149.23px" class="cls_006"><span class="cls_006">scheduling to run.</span></div>
<div style="position:absolute;left:85.27px;top:192.63px" class="cls_006"><span class="cls_006">If a thread of a higher priority enters the runnable</span></div>
<div style="position:absolute;left:85.27px;top:221.23px" class="cls_006"><span class="cls_006">set, the currently running thread will be </span><span class="cls_007">preempted</span></div>
<div style="position:absolute;left:85.27px;top:250.23px" class="cls_006"><span class="cls_006">by the new thread.</span></div>
<div style="position:absolute;left:83.27px;top:543.60px" class="cls_002"><span class="cls_002">Advanced Topics in Software Engineering</span></div>
<div style="position:absolute;left:659.27px;top:543.60px" class="cls_002"><span class="cls_002">15</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:9330px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="javathreads/background16.jpg" width=792 height=612></div>
<div style="position:absolute;left:85.27px;top:60.08px" class="cls_019"><span class="cls_019">Java Threads</span></div>
<div style="position:absolute;left:85.27px;top:121.23px" class="cls_004"><span class="cls_004">q</span><span class="cls_005"> </span><span class="cls_006">Thread Creation</span></div>
<div style="position:absolute;left:85.27px;top:163.63px" class="cls_004"><span class="cls_004">q</span><span class="cls_005"> </span><span class="cls_006">Thread Synchronization</span></div>
<div style="position:absolute;left:85.27px;top:207.63px" class="cls_004"><span class="cls_004">q</span><span class="cls_005"> </span><span class="cls_006">Thread States And Scheduling</span></div>
<div style="position:absolute;left:85.27px;top:250.63px" class="cls_004"><span class="cls_004">q</span><span class="cls_005"> </span><span class="cls_007">Short Demo</span></div>
<div style="position:absolute;left:83.27px;top:543.60px" class="cls_002"><span class="cls_002">Advanced Topics in Software Engineering</span></div>
<div style="position:absolute;left:659.27px;top:543.60px" class="cls_002"><span class="cls_002">16</span></div>
</div>

</body>
</html>
