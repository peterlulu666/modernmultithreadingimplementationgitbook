# [Modern Multithreading : Implementing, Testing, and Debugging Multithreaded Java and C++/Pthreads/Win32 Programs](https://www.amazon.com/Modern-Multithreading-Implementing-Debugging-Multithreaded/dp/0471725048/ref=sr_1_3?crid=2VSMVR3UZ8OLD&keywords=modern+multithreading&qid=1642608918&sprefix=modern+multithreading%2Caps%2C68&sr=8-3)

## Syllabus

<iframe 
height="842"
width="100%"
src="2022-SPRING_2222-CSE-6324-002.pdf"></iframe>

