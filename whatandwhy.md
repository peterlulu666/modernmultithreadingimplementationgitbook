<html>
<head><meta http-equiv=Content-Type content="text/html; charset=UTF-8">
<style type="text/css">
<!--
span.cls_003{font-family:Arial,serif;font-size:38.0px;color:rgb(51,51,204);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_003{font-family:Arial,serif;font-size:38.0px;color:rgb(51,51,204);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_004{font-family:Arial,serif;font-size:30.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_004{font-family:Arial,serif;font-size:30.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_015{font-family:Arial,serif;font-size:30.1px;color:rgb(0,0,255);font-weight:normal;font-style:normal;text-decoration: underline}
div.cls_015{font-family:Arial,serif;font-size:30.1px;color:rgb(0,0,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_002{font-family:Times,serif;font-size:13.0px;color:rgb(0,0,255);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_002{font-family:Times,serif;font-size:13.0px;color:rgb(0,0,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_006{font-family:Times,serif;font-size:43.1px;color:rgb(0,0,255);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_006{font-family:Times,serif;font-size:43.1px;color:rgb(0,0,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_007{font-family:Arial,serif;font-size:30.1px;color:rgb(51,51,204);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_007{font-family:Arial,serif;font-size:30.1px;color:rgb(51,51,204);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_008{font-family:Arial,serif;font-size:26.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_008{font-family:Arial,serif;font-size:26.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_009{font-family:Arial,serif;font-size:13.0px;color:rgb(0,0,255);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_009{font-family:Arial,serif;font-size:13.0px;color:rgb(0,0,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_010{font-family:Arial,serif;font-size:26.0px;color:rgb(51,51,204);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_010{font-family:Arial,serif;font-size:26.0px;color:rgb(51,51,204);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_011{font-family:Times,serif;font-size:26.0px;color:rgb(0,127,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_011{font-family:Times,serif;font-size:26.0px;color:rgb(0,127,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_016{font-family:Times,serif;font-size:26.0px;color:rgb(0,0,255);font-weight:normal;font-style:normal;text-decoration: underline}
div.cls_016{font-family:Times,serif;font-size:26.0px;color:rgb(0,0,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_013{font-family:Arial,serif;font-size:34.1px;color:rgb(51,51,204);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_013{font-family:Arial,serif;font-size:34.1px;color:rgb(51,51,204);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_014{font-family:Arial,serif;font-size:15.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_014{font-family:Arial,serif;font-size:15.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
-->
</style>
<script type="text/javascript" src="whatandwhy/wz_jsgraphics.js"></script>
</head>
<body>
<div style="position:absolute;left:50%;margin-left:-396px;top:0px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="whatandwhy/background1.jpg" width=792 height=612></div>
<div style="position:absolute;left:66.07px;top:211.60px" class="cls_003"><span class="cls_003">Software Engineering: What and Why</span></div>
<div style="position:absolute;left:320.10px;top:378.96px" class="cls_004"><span class="cls_004">Jeff Lei</span></div>
<div style="position:absolute;left:257.50px;top:414.96px" class="cls_015"><span class="cls_015"> </span><A HREF="mailto:ylei@cse.uta.edu">ylei@cse.uta.ed</A>u</div>
<div style="position:absolute;left:291.70px;top:450.96px" class="cls_004"><span class="cls_004">Spring 2022</span></div>
<div style="position:absolute;left:708.21px;top:558.44px" class="cls_002"><span class="cls_002">1</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:622px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="whatandwhy/background2.jpg" width=792 height=612></div>
<div style="position:absolute;left:244.07px;top:38.36px" class="cls_006"><span class="cls_006">Software Engineering</span></div>
<div style="position:absolute;left:77.28px;top:136.08px" class="cls_004"><span class="cls_004">• Software has become </span><span class="cls_007">pervasive</span><span class="cls_004"> in modern</span></div>
<div style="position:absolute;left:106.08px;top:172.08px" class="cls_004"><span class="cls_004">society</span></div>
<div style="position:absolute;left:115.68px;top:213.04px" class="cls_008"><span class="cls_008">- A key enabler to technology and society</span></div>
<div style="position:absolute;left:139.68px;top:244.00px" class="cls_008"><span class="cls_008">advancement</span></div>
<div style="position:absolute;left:77.28px;top:281.04px" class="cls_004"><span class="cls_004">• How to build </span><span class="cls_007">better</span><span class="cls_004"> software </span><span class="cls_007">faster</span><span class="cls_004">, especially</span></div>
<div style="position:absolute;left:106.08px;top:317.04px" class="cls_007"><span class="cls_007">at scale</span><span class="cls_004">?</span></div>
<div style="position:absolute;left:115.68px;top:358.00px" class="cls_008"><span class="cls_008">- Requirements, analysis, design, coding, testing,</span></div>
<div style="position:absolute;left:139.68px;top:388.96px" class="cls_008"><span class="cls_008">maintenance, configuration, documentation,</span></div>
<div style="position:absolute;left:139.68px;top:419.92px" class="cls_008"><span class="cls_008">deployment, etc.</span></div>
<div style="position:absolute;left:708.21px;top:558.44px" class="cls_002"><span class="cls_002">2</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:1244px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="whatandwhy/background3.jpg" width=792 height=612></div>
<div style="position:absolute;left:213.80px;top:38.36px" class="cls_006"><span class="cls_006">Software Engineering (2)</span></div>
<div style="position:absolute;left:77.28px;top:136.08px" class="cls_007"><span class="cls_007">• Concepts</span><span class="cls_004">, </span><span class="cls_007">principles</span><span class="cls_004">, </span><span class="cls_007">models</span><span class="cls_004">, </span><span class="cls_007">methods</span><span class="cls_004">,</span></div>
<div style="position:absolute;left:106.08px;top:172.08px" class="cls_007"><span class="cls_007">techniques</span><span class="cls_004">, and </span><span class="cls_007">tools</span><span class="cls_004"> that make software</span></div>
<div style="position:absolute;left:106.08px;top:208.08px" class="cls_004"><span class="cls_004">engineers more productive</span></div>
<div style="position:absolute;left:77.28px;top:251.04px" class="cls_004"><span class="cls_004">• In other words, </span><span class="cls_007">Software Engineering </span><span class="cls_004">helps</span></div>
<div style="position:absolute;left:106.08px;top:287.04px" class="cls_004"><span class="cls_004">software engineers to do their jobs better …</span></div>
<div style="position:absolute;left:77.28px;top:330.00px" class="cls_004"><span class="cls_004">•</span></div>
<div style="position:absolute;left:114.35px;top:330.00px" class="cls_004"><span class="cls_004">… and in turn software engineers help other</span></div>
<div style="position:absolute;left:106.08px;top:366.00px" class="cls_004"><span class="cls_004">people to do their jobs better.</span></div>
<div style="position:absolute;left:707.55px;top:558.44px" class="cls_009"><span class="cls_009">3</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:1866px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="whatandwhy/background4.jpg" width=792 height=612></div>
<div style="position:absolute;left:284.33px;top:38.36px" class="cls_006"><span class="cls_006">Software Quality</span></div>
<div style="position:absolute;left:77.28px;top:136.08px" class="cls_007"><span class="cls_007">• THE</span><span class="cls_004"> priority concern in software engineering</span></div>
<div style="position:absolute;left:115.68px;top:177.04px" class="cls_008"><span class="cls_008">- No quality, no engineering!</span></div>
<div style="position:absolute;left:115.68px;top:213.04px" class="cls_008"><span class="cls_008">- Software malfunctions cost billions of dollars every</span></div>
<div style="position:absolute;left:139.68px;top:244.00px" class="cls_008"><span class="cls_008">year, and have severe consequences in a safe-</span></div>
<div style="position:absolute;left:139.68px;top:274.96px" class="cls_008"><span class="cls_008">critical environment</span></div>
<div style="position:absolute;left:77.28px;top:312.00px" class="cls_004"><span class="cls_004">• An important factor that distinguishes a</span></div>
<div style="position:absolute;left:106.08px;top:348.00px" class="cls_004"><span class="cls_004">software product from its competition</span></div>
<div style="position:absolute;left:115.68px;top:388.96px" class="cls_008"><span class="cls_008">- The feature set tends to converge between similar</span></div>
<div style="position:absolute;left:139.68px;top:419.92px" class="cls_008"><span class="cls_008">products</span></div>
<div style="position:absolute;left:708.21px;top:558.44px" class="cls_002"><span class="cls_002">4</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:2488px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="whatandwhy/background5.jpg" width=792 height=612></div>
<div style="position:absolute;left:286.13px;top:38.36px" class="cls_006"><span class="cls_006">What is Quality?</span></div>
<div style="position:absolute;left:77.28px;top:136.08px" class="cls_004"><span class="cls_004">• As perceived by the user</span></div>
<div style="position:absolute;left:115.68px;top:177.04px" class="cls_008"><span class="cls_008">- Functionally correct, secure, robust, reliable,</span></div>
<div style="position:absolute;left:139.68px;top:208.00px" class="cls_008"><span class="cls_008">usable, fast response, high throughput, …</span></div>
<div style="position:absolute;left:77.28px;top:245.04px" class="cls_004"><span class="cls_004">• As perceived by the developer</span></div>
<div style="position:absolute;left:115.68px;top:286.96px" class="cls_008"><span class="cls_008">- Code that is well-structured, easy to understand,</span></div>
<div style="position:absolute;left:139.68px;top:316.96px" class="cls_008"><span class="cls_008">easy to change, easy to extend, easy to test, …</span></div>
<div style="position:absolute;left:77.28px;top:354.96px" class="cls_004"><span class="cls_004">• Software = program + documentation</span></div>
<div style="position:absolute;left:115.68px;top:395.92px" class="cls_008"><span class="cls_008">- Concise, well-written, consistent, up-to-date</span></div>
<div style="position:absolute;left:708.21px;top:558.44px" class="cls_002"><span class="cls_002">5</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:3110px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="whatandwhy/background6.jpg" width=792 height=612></div>
<div style="position:absolute;left:179.33px;top:38.36px" class="cls_006"><span class="cls_006">On-the-large vs On-the-small</span></div>
<div style="position:absolute;left:77.28px;top:136.08px" class="cls_007"><span class="cls_007">• On-the-large</span><span class="cls_004">: Manage the overall production</span></div>
<div style="position:absolute;left:106.08px;top:172.08px" class="cls_004"><span class="cls_004">process</span></div>
<div style="position:absolute;left:115.68px;top:214.00px" class="cls_008"><span class="cls_008">- Software process and methodology</span></div>
<div style="position:absolute;left:115.68px;top:250.96px" class="cls_008"><span class="cls_008">- Project management, i.e., planning & scheduling</span></div>
<div style="position:absolute;left:77.28px;top:288.96px" class="cls_007"><span class="cls_007">• On-the-small</span><span class="cls_004">: Focus on the “</span><span class="cls_007">how-to</span><span class="cls_004">” of</span></div>
<div style="position:absolute;left:106.08px;top:324.96px" class="cls_004"><span class="cls_004">individual tasks</span></div>
<div style="position:absolute;left:115.68px;top:366.88px" class="cls_008"><span class="cls_008">- Requirement engineering, design patterns, design</span></div>
<div style="position:absolute;left:139.68px;top:398.08px" class="cls_008"><span class="cls_008">verification, program analysis, debugging, testing,</span></div>
<div style="position:absolute;left:139.68px;top:429.04px" class="cls_008"><span class="cls_008">formal methods, and others</span></div>
<div style="position:absolute;left:707.55px;top:558.44px" class="cls_009"><span class="cls_009">6</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:3732px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="whatandwhy/background7.jpg" width=792 height=612></div>
<div style="position:absolute;left:222.67px;top:38.36px" class="cls_006"><span class="cls_006">Defining Characteristics</span></div>
<div style="position:absolute;left:77.28px;top:136.08px" class="cls_004"><span class="cls_004">• Deals with </span><span class="cls_007">semantics</span><span class="cls_004"> instead of </span><span class="cls_007">syntax</span></div>
<div style="position:absolute;left:115.68px;top:178.00px" class="cls_008"><span class="cls_008">- Semantic reasoning is often </span><span class="cls_010">undecidable</span><span class="cls_008">!!</span></div>
<div style="position:absolute;left:77.28px;top:216.00px" class="cls_004"><span class="cls_004">• Analyzes </span><span class="cls_007">code</span><span class="cls_004"> instead of </span><span class="cls_007">data</span></div>
<div style="position:absolute;left:115.68px;top:257.92px" class="cls_008"><span class="cls_008">- How to analyze the runtime behavior that may be</span></div>
<div style="position:absolute;left:139.68px;top:288.88px" class="cls_008"><span class="cls_008">exercised by (static) code?</span></div>
<div style="position:absolute;left:77.28px;top:326.88px" class="cls_004"><span class="cls_004">• Considers the </span><span class="cls_007">general</span><span class="cls_004"> software production</span></div>
<div style="position:absolute;left:106.08px;top:362.88px" class="cls_004"><span class="cls_004">process/activities, instead of building a</span></div>
<div style="position:absolute;left:106.08px;top:398.88px" class="cls_004"><span class="cls_004">specific system.</span></div>
<div style="position:absolute;left:77.28px;top:441.84px" class="cls_004"><span class="cls_004">• The goal is to ensure </span><span class="cls_007">quality of software</span></div>
<div style="position:absolute;left:106.08px;top:478.08px" class="cls_007"><span class="cls_007">products.</span></div>
<div style="position:absolute;left:707.55px;top:558.44px" class="cls_009"><span class="cls_009">7</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:4354px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="whatandwhy/background8.jpg" width=792 height=612></div>
<div style="position:absolute;left:227.93px;top:38.36px" class="cls_006"><span class="cls_006">Quotes from Dr. Parnas</span></div>
<div style="position:absolute;left:186.08px;top:430.48px" class="cls_011"><span class="cls_011">Extracted from his ACM Fellow Profile</span></div>
<div style="position:absolute;left:186.08px;top:461.44px" class="cls_016"><span class="cls_016"> </span><A HREF="http://www.sigsoft.org/SEN/parnas.html">http://www.sigsoft.org/SEN/parnas.htm</A>l</div>
<div style="position:absolute;left:708.21px;top:558.44px" class="cls_002"><span class="cls_002">8</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-396px;top:4976px;width:792px;height:612px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="whatandwhy/background9.jpg" width=792 height=612></div>
<div style="position:absolute;left:92.94px;top:226.88px" class="cls_013"><span class="cls_013">Education is what remains after one has</span></div>
<div style="position:absolute;left:83.53px;top:267.68px" class="cls_013"><span class="cls_013">forgotten everything he learned in school.</span></div>
<div style="position:absolute;left:271.07px;top:315.72px" class="cls_014"><span class="cls_014">- Albert Einstein (or an unnamed wit?)</span></div>
<div style="position:absolute;left:708.21px;top:558.44px" class="cls_002"><span class="cls_002">9</span></div>
</div>

</body>
</html>
